package net.minecraft.server;

public class ii extends il<EntityTypes<?>> {

    public ii(DebugReportGenerator debugreportgenerator) {
        super(debugreportgenerator, IRegistry.ENTITY_TYPE);
    }

    @Override
    protected void b() {
        this.a(TagsEntity.SKELETONS).a((Object[])(EntityTypes.SKELETON, EntityTypes.STRAY, EntityTypes.WITHER_SKELETON));
        this.a(TagsEntity.RADIERS).a((Object[])(EntityTypes.EVOKER, EntityTypes.PILLAGER, EntityTypes.RAVAGER, EntityTypes.VINDICATOR, EntityTypes.ILLUSIONER, EntityTypes.WITCH));
        this.a(TagsEntity.BEEHIVE_INHABITORS).a((Object)EntityTypes.BEE);
        this.a(TagsEntity.ARROWS).a((Object[])(EntityTypes.ARROW, EntityTypes.SPECTRAL_ARROW));
    }

    @Override
    protected java.nio.file.Path a(MinecraftKey minecraftkey) {
        return this.b.b().resolve("data/" + minecraftkey.getNamespace() + "/tags/entity_types/" + minecraftkey.getKey() + ".json");
    }

    @Override
    public String a() {
        return "Entity Type Tags";
    }

    @Override
    protected void a(Tags<EntityTypes<?>> tags) {
        TagsEntity.a(tags);
    }
}
