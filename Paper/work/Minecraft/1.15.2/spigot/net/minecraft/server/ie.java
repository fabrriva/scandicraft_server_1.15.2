package net.minecraft.server;

public class ie implements id.a {

    public ie() {}

    @Override
    public NBTTagCompound a(String s, NBTTagCompound nbttagcompound) {
        return s.startsWith("data/minecraft/structures/") ? b(a(nbttagcompound)) : nbttagcompound;
    }

    private static NBTTagCompound a(NBTTagCompound nbttagcompound) {
        if (!nbttagcompound.hasKeyOfType("DataVersion", 99)) {
            nbttagcompound.setInt("DataVersion", 500);
        }

        return nbttagcompound;
    }

    private static NBTTagCompound b(NBTTagCompound nbttagcompound) {
        DefinedStructure definedstructure = new DefinedStructure();

        definedstructure.b(GameProfileSerializer.a(DataConverterRegistry.a(), DataFixTypes.STRUCTURE, nbttagcompound, nbttagcompound.getInt("DataVersion")));
        return definedstructure.a(new NBTTagCompound());
    }
}
