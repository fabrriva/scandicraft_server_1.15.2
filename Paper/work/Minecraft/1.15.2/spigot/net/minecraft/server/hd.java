package net.minecraft.server;

import java.util.function.Consumer;

public class hd implements Consumer<Consumer<Advancement>> {

    private static final EntityTypes<?>[] a = new EntityTypes[]{EntityTypes.HORSE, EntityTypes.SHEEP, EntityTypes.COW, EntityTypes.MOOSHROOM, EntityTypes.PIG, EntityTypes.CHICKEN, EntityTypes.WOLF, EntityTypes.OCELOT, EntityTypes.RABBIT, EntityTypes.LLAMA, EntityTypes.TURTLE, EntityTypes.CAT, EntityTypes.PANDA, EntityTypes.FOX, EntityTypes.BEE};
    private static final Item[] b = new Item[]{Items.COD, Items.TROPICAL_FISH, Items.PUFFERFISH, Items.SALMON};
    private static final Item[] c = new Item[]{Items.COD_BUCKET, Items.TROPICAL_FISH_BUCKET, Items.PUFFERFISH_BUCKET, Items.SALMON_BUCKET};
    private static final Item[] d = new Item[]{Items.APPLE, Items.MUSHROOM_STEW, Items.BREAD, Items.PORKCHOP, Items.COOKED_PORKCHOP, Items.GOLDEN_APPLE, Items.ENCHANTED_GOLDEN_APPLE, Items.COD, Items.SALMON, Items.TROPICAL_FISH, Items.PUFFERFISH, Items.COOKED_COD, Items.COOKED_SALMON, Items.COOKIE, Items.MELON_SLICE, Items.BEEF, Items.COOKED_BEEF, Items.CHICKEN, Items.COOKED_CHICKEN, Items.ROTTEN_FLESH, Items.SPIDER_EYE, Items.CARROT, Items.POTATO, Items.BAKED_POTATO, Items.POISONOUS_POTATO, Items.GOLDEN_CARROT, Items.PUMPKIN_PIE, Items.RABBIT, Items.COOKED_RABBIT, Items.RABBIT_STEW, Items.MUTTON, Items.COOKED_MUTTON, Items.CHORUS_FRUIT, Items.BEETROOT, Items.BEETROOT_SOUP, Items.DRIED_KELP, Items.SUSPICIOUS_STEW, Items.SWEET_BERRIES, Items.pX};

    public hd() {}

    public void accept(Consumer<Advancement> consumer) {
        Advancement advancement = Advancement.SerializedAdvancement.a().a((IMaterial) Blocks.HAY_BLOCK, new ChatMessage("advancements.husbandry.root.title", new Object[0]), new ChatMessage("advancements.husbandry.root.description", new Object[0]), new MinecraftKey("textures/gui/advancements/backgrounds/husbandry.png"), AdvancementFrameType.TASK, false, false, false).a("consumed_item", (CriterionInstance) CriterionTriggerConsumeItem.a.c()).a(consumer, "husbandry/root");
        Advancement advancement1 = Advancement.SerializedAdvancement.a().a(advancement).a((IMaterial) Items.WHEAT, new ChatMessage("advancements.husbandry.plant_seed.title", new Object[0]), new ChatMessage("advancements.husbandry.plant_seed.description", new Object[0]), (MinecraftKey) null, AdvancementFrameType.TASK, true, true, false).a(AdvancementRequirements.OR).a("wheat", (CriterionInstance) CriterionTriggerPlacedBlock.a.a(Blocks.WHEAT)).a("pumpkin_stem", (CriterionInstance) CriterionTriggerPlacedBlock.a.a(Blocks.PUMPKIN_STEM)).a("melon_stem", (CriterionInstance) CriterionTriggerPlacedBlock.a.a(Blocks.MELON_STEM)).a("beetroots", (CriterionInstance) CriterionTriggerPlacedBlock.a.a(Blocks.BEETROOTS)).a("nether_wart", (CriterionInstance) CriterionTriggerPlacedBlock.a.a(Blocks.NETHER_WART)).a(consumer, "husbandry/plant_seed");
        Advancement advancement2 = Advancement.SerializedAdvancement.a().a(advancement).a((IMaterial) Items.WHEAT, new ChatMessage("advancements.husbandry.breed_an_animal.title", new Object[0]), new ChatMessage("advancements.husbandry.breed_an_animal.description", new Object[0]), (MinecraftKey) null, AdvancementFrameType.TASK, true, true, false).a(AdvancementRequirements.OR).a("bred", (CriterionInstance) CriterionTriggerBredAnimals.a.c()).a(consumer, "husbandry/breed_an_animal");
        Advancement advancement3 = this.a(Advancement.SerializedAdvancement.a()).a(advancement1).a((IMaterial) Items.APPLE, new ChatMessage("advancements.husbandry.balanced_diet.title", new Object[0]), new ChatMessage("advancements.husbandry.balanced_diet.description", new Object[0]), (MinecraftKey) null, AdvancementFrameType.CHALLENGE, true, true, false).a(AdvancementRewards.a.a(100)).a(consumer, "husbandry/balanced_diet");
        Advancement advancement4 = Advancement.SerializedAdvancement.a().a(advancement1).a((IMaterial) Items.DIAMOND_HOE, new ChatMessage("advancements.husbandry.break_diamond_hoe.title", new Object[0]), new ChatMessage("advancements.husbandry.break_diamond_hoe.description", new Object[0]), (MinecraftKey) null, AdvancementFrameType.CHALLENGE, true, true, false).a(AdvancementRewards.a.a(100)).a("broke_hoe", (CriterionInstance) CriterionTriggerItemDurabilityChanged.a.a(CriterionConditionItem.a.a().a((IMaterial) Items.DIAMOND_HOE).b(), CriterionConditionValue.IntegerRange.a(0))).a(consumer, "husbandry/break_diamond_hoe");
        Advancement advancement5 = Advancement.SerializedAdvancement.a().a(advancement).a((IMaterial) Items.LEAD, new ChatMessage("advancements.husbandry.tame_an_animal.title", new Object[0]), new ChatMessage("advancements.husbandry.tame_an_animal.description", new Object[0]), (MinecraftKey) null, AdvancementFrameType.TASK, true, true, false).a("tamed_animal", (CriterionInstance) CriterionTriggerTamedAnimal.a.c()).a(consumer, "husbandry/tame_an_animal");
        Advancement advancement6 = this.b(Advancement.SerializedAdvancement.a()).a(advancement2).a((IMaterial) Items.GOLDEN_CARROT, new ChatMessage("advancements.husbandry.breed_all_animals.title", new Object[0]), new ChatMessage("advancements.husbandry.breed_all_animals.description", new Object[0]), (MinecraftKey) null, AdvancementFrameType.CHALLENGE, true, true, false).a(AdvancementRewards.a.a(100)).a(consumer, "husbandry/bred_all_animals");
        Advancement advancement7 = this.d(Advancement.SerializedAdvancement.a()).a(advancement).a(AdvancementRequirements.OR).a((IMaterial) Items.FISHING_ROD, new ChatMessage("advancements.husbandry.fishy_business.title", new Object[0]), new ChatMessage("advancements.husbandry.fishy_business.description", new Object[0]), (MinecraftKey) null, AdvancementFrameType.TASK, true, true, false).a(consumer, "husbandry/fishy_business");
        Advancement advancement8 = this.c(Advancement.SerializedAdvancement.a()).a(advancement7).a(AdvancementRequirements.OR).a((IMaterial) Items.PUFFERFISH_BUCKET, new ChatMessage("advancements.husbandry.tactical_fishing.title", new Object[0]), new ChatMessage("advancements.husbandry.tactical_fishing.description", new Object[0]), (MinecraftKey) null, AdvancementFrameType.TASK, true, true, false).a(consumer, "husbandry/tactical_fishing");
        Advancement advancement9 = this.e(Advancement.SerializedAdvancement.a()).a(advancement5).a((IMaterial) Items.COD, new ChatMessage("advancements.husbandry.complete_catalogue.title", new Object[0]), new ChatMessage("advancements.husbandry.complete_catalogue.description", new Object[0]), (MinecraftKey) null, AdvancementFrameType.CHALLENGE, true, true, false).a(AdvancementRewards.a.a(50)).a(consumer, "husbandry/complete_catalogue");
        Advancement advancement10 = Advancement.SerializedAdvancement.a().a(advancement).a("safely_harvest_honey", (CriterionInstance) CriterionTriggerInteractBlock.a.a(CriterionConditionBlock.a.a().a(TagsBlock.BEEHIVES), CriterionConditionItem.a.a().a((IMaterial) Items.GLASS_BOTTLE))).a((IMaterial) Items.pX, new ChatMessage("advancements.husbandry.safely_harvest_honey.title", new Object[0]), new ChatMessage("advancements.husbandry.safely_harvest_honey.description", new Object[0]), (MinecraftKey) null, AdvancementFrameType.TASK, true, true, false).a(consumer, "husbandry/safely_harvest_honey");
        Advancement advancement11 = Advancement.SerializedAdvancement.a().a(advancement).a("silk_touch_nest", (CriterionInstance) CriterionTriggerBeeNestDestroyed.a.a(Blocks.BEE_NEST, CriterionConditionItem.a.a().a(new CriterionConditionEnchantments(Enchantments.SILK_TOUCH, CriterionConditionValue.IntegerRange.b(1))), CriterionConditionValue.IntegerRange.a(3))).a((IMaterial) Blocks.BEE_NEST, new ChatMessage("advancements.husbandry.silk_touch_nest.title", new Object[0]), new ChatMessage("advancements.husbandry.silk_touch_nest.description", new Object[0]), (MinecraftKey) null, AdvancementFrameType.TASK, true, true, false).a(consumer, "husbandry/silk_touch_nest");
    }

    private Advancement.SerializedAdvancement a(Advancement.SerializedAdvancement advancement_serializedadvancement) {
        Item[] aitem = hd.d;
        int i = aitem.length;

        for (int j = 0; j < i; ++j) {
            Item item = aitem[j];

            advancement_serializedadvancement.a(IRegistry.ITEM.getKey(item).getKey(), (CriterionInstance) CriterionTriggerConsumeItem.a.a((IMaterial) item));
        }

        return advancement_serializedadvancement;
    }

    private Advancement.SerializedAdvancement b(Advancement.SerializedAdvancement advancement_serializedadvancement) {
        EntityTypes[] aentitytypes = hd.a;
        int i = aentitytypes.length;

        for (int j = 0; j < i; ++j) {
            EntityTypes<?> entitytypes = aentitytypes[j];

            advancement_serializedadvancement.a(EntityTypes.getName(entitytypes).toString(), (CriterionInstance) CriterionTriggerBredAnimals.a.a(CriterionConditionEntity.a.a().a(entitytypes)));
        }

        return advancement_serializedadvancement;
    }

    private Advancement.SerializedAdvancement c(Advancement.SerializedAdvancement advancement_serializedadvancement) {
        Item[] aitem = hd.c;
        int i = aitem.length;

        for (int j = 0; j < i; ++j) {
            Item item = aitem[j];

            advancement_serializedadvancement.a(IRegistry.ITEM.getKey(item).getKey(), (CriterionInstance) CriterionTriggerFilledBucket.a.a(CriterionConditionItem.a.a().a((IMaterial) item).b()));
        }

        return advancement_serializedadvancement;
    }

    private Advancement.SerializedAdvancement d(Advancement.SerializedAdvancement advancement_serializedadvancement) {
        Item[] aitem = hd.b;
        int i = aitem.length;

        for (int j = 0; j < i; ++j) {
            Item item = aitem[j];

            advancement_serializedadvancement.a(IRegistry.ITEM.getKey(item).getKey(), (CriterionInstance) CriterionTriggerFishingRodHooked.a.a(CriterionConditionItem.a, CriterionConditionEntity.a, CriterionConditionItem.a.a().a((IMaterial) item).b()));
        }

        return advancement_serializedadvancement;
    }

    private Advancement.SerializedAdvancement e(Advancement.SerializedAdvancement advancement_serializedadvancement) {
        EntityCat.bz.forEach((integer, minecraftkey) -> {
            advancement_serializedadvancement.a(minecraftkey.getKey(), (CriterionInstance) CriterionTriggerTamedAnimal.a.a(CriterionConditionEntity.a.a().a(minecraftkey).b()));
        });
        return advancement_serializedadvancement;
    }
}
