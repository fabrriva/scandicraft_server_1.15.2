package net.minecraft.server;

import com.google.gson.JsonObject;
import java.util.function.Consumer;
import javax.annotation.Nullable;

public class ia {

    private final RecipeSerializerComplex<?> a;

    public ia(RecipeSerializerComplex<?> recipeserializercomplex) {
        this.a = recipeserializercomplex;
    }

    public static ia a(RecipeSerializerComplex<?> recipeserializercomplex) {
        return new ia(recipeserializercomplex);
    }

    public void a(Consumer<hu> consumer, final String s) {
        consumer.accept(new hu() {
            @Override
            public void a(JsonObject jsonobject) {}

            @Override
            public RecipeSerializer<?> c() {
                return ia.this.a;
            }

            @Override
            public MinecraftKey b() {
                return new MinecraftKey(s);
            }

            @Nullable
            @Override
            public JsonObject d() {
                return null;
            }

            @Override
            public MinecraftKey e() {
                return new MinecraftKey("");
            }
        });
    }
}
