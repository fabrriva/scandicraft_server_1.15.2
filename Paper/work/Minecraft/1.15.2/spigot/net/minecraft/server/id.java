package net.minecraft.server;

import com.google.common.collect.Lists;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import javax.annotation.Nullable;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class id implements DebugReportProvider {

    private static final Logger LOGGER = LogManager.getLogger();
    private final DebugReportGenerator c;
    private final List<id.a> d = Lists.newArrayList();

    public id(DebugReportGenerator debugreportgenerator) {
        this.c = debugreportgenerator;
    }

    public id a(id.a id_a) {
        this.d.add(id_a);
        return this;
    }

    private NBTTagCompound a(String s, NBTTagCompound nbttagcompound) {
        NBTTagCompound nbttagcompound1 = nbttagcompound;

        id.a id_a;

        for (Iterator iterator = this.d.iterator(); iterator.hasNext(); nbttagcompound1 = id_a.a(s, nbttagcompound1)) {
            id_a = (id.a) iterator.next();
        }

        return nbttagcompound1;
    }

    @Override
    public void a(HashCache hashcache) throws IOException {
        java.nio.file.Path java_nio_file_path = this.c.b();
        List<CompletableFuture<id.b>> list = Lists.newArrayList();
        Iterator iterator = this.c.a().iterator();

        while (iterator.hasNext()) {
            java.nio.file.Path java_nio_file_path1 = (java.nio.file.Path) iterator.next();

            Files.walk(java_nio_file_path1).filter((java_nio_file_path2) -> {
                return java_nio_file_path2.toString().endsWith(".snbt");
            }).forEach((java_nio_file_path2) -> {
                list.add(CompletableFuture.supplyAsync(() -> {
                    return this.a(java_nio_file_path2, this.a(java_nio_file_path1, java_nio_file_path2));
                }, SystemUtils.e()));
            });
        }

        ((List) SystemUtils.b(list).join()).stream().filter(Objects::nonNull).forEach((id_b) -> {
            this.a(hashcache, id_b, java_nio_file_path);
        });
    }

    @Override
    public String a() {
        return "SNBT -> NBT";
    }

    private String a(java.nio.file.Path java_nio_file_path, java.nio.file.Path java_nio_file_path1) {
        String s = java_nio_file_path.relativize(java_nio_file_path1).toString().replaceAll("\\\\", "/");

        return s.substring(0, s.length() - ".snbt".length());
    }

    @Nullable
    private id.b a(java.nio.file.Path java_nio_file_path, String s) {
        try {
            BufferedReader bufferedreader = Files.newBufferedReader(java_nio_file_path);
            Throwable throwable = null;

            id.b id_b;

            try {
                String s1 = IOUtils.toString(bufferedreader);
                ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream();

                NBTCompressedStreamTools.a(this.a(s, MojangsonParser.parse(s1)), (OutputStream) bytearrayoutputstream);
                byte[] abyte = bytearrayoutputstream.toByteArray();
                String s2 = id.a.hashBytes(abyte).toString();

                id_b = new id.b(s, abyte, s2);
            } catch (Throwable throwable1) {
                throwable = throwable1;
                throw throwable1;
            } finally {
                if (bufferedreader != null) {
                    if (throwable != null) {
                        try {
                            bufferedreader.close();
                        } catch (Throwable throwable2) {
                            throwable.addSuppressed(throwable2);
                        }
                    } else {
                        bufferedreader.close();
                    }
                }

            }

            return id_b;
        } catch (CommandSyntaxException commandsyntaxexception) {
            id.LOGGER.error("Couldn't convert {} from SNBT to NBT at {} as it's invalid SNBT", s, java_nio_file_path, commandsyntaxexception);
        } catch (IOException ioexception) {
            id.LOGGER.error("Couldn't convert {} from SNBT to NBT at {}", s, java_nio_file_path, ioexception);
        }

        return null;
    }

    private void a(HashCache hashcache, id.b id_b, java.nio.file.Path java_nio_file_path) {
        java.nio.file.Path java_nio_file_path1 = java_nio_file_path.resolve(id_b.a + ".nbt");

        try {
            if (!Objects.equals(hashcache.a(java_nio_file_path1), id_b.c) || !Files.exists(java_nio_file_path1, new LinkOption[0])) {
                Files.createDirectories(java_nio_file_path1.getParent());
                OutputStream outputstream = Files.newOutputStream(java_nio_file_path1);
                Throwable throwable = null;

                try {
                    outputstream.write(id_b.b);
                } catch (Throwable throwable1) {
                    throwable = throwable1;
                    throw throwable1;
                } finally {
                    if (outputstream != null) {
                        if (throwable != null) {
                            try {
                                outputstream.close();
                            } catch (Throwable throwable2) {
                                throwable.addSuppressed(throwable2);
                            }
                        } else {
                            outputstream.close();
                        }
                    }

                }
            }

            hashcache.a(java_nio_file_path1, id_b.c);
        } catch (IOException ioexception) {
            id.LOGGER.error("Couldn't write structure {} at {}", id_b.a, java_nio_file_path1, ioexception);
        }

    }

    @FunctionalInterface
    public interface a {

        NBTTagCompound a(String s, NBTTagCompound nbttagcompound);
    }

    static class b {

        private final String a;
        private final byte[] b;
        private final String c;

        public b(String s, byte[] abyte, String s1) {
            this.a = s;
            this.b = abyte;
            this.c = s1;
        }
    }
}
