package fr.scandicraft.items;

import net.minecraft.server.Item;
import net.minecraft.server.ItemStack;

public class ItemGoldenAppleEnchanted extends Item {

    public ItemGoldenAppleEnchanted(Item.Info item_info) {
        super(item_info);
    }

    // client: hasEffect
    public boolean d_(ItemStack itemstack) {
        return true;
    }
}
