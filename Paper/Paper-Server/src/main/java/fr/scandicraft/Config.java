package fr.scandicraft;

public class Config {

    /* Variables */
    public static int ENDERPEARL_COOLDOWN = 20;	//20 ticks = 1 secondes
    public static int ENCHANTEDGOLDEN_APPLE_COOLDOWN = 600;	//600 ticks = 30 secondes
    public static int GOLDEN_APPLE_COOLDOWN = 300;	//300 ticks = 15 secondes

}
