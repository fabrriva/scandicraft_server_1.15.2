//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package net.minecraft.server;

import net.minecraft.server.FoodInfo.a;

public class Foods {
    public static final FoodInfo a = (new a()).a(4).a(0.3F).d();
    public static final FoodInfo b = (new a()).a(5).a(0.6F).d();
    public static final FoodInfo c = (new a()).a(3).a(0.3F).a().d();
    public static final FoodInfo d = (new a()).a(1).a(0.6F).d();
    public static final FoodInfo e = a(6);
    public static final FoodInfo f = (new a()).a(5).a(0.6F).d();
    public static final FoodInfo g = (new a()).a(3).a(0.6F).d();
    public static final FoodInfo h;
    public static final FoodInfo i;
    public static final FoodInfo j;
    public static final FoodInfo k;
    public static final FoodInfo l;
    public static final FoodInfo m;
    public static final FoodInfo n;
    public static final FoodInfo o;
    public static final FoodInfo p;
    public static final FoodInfo q;
    public static final FoodInfo r;
    public static final FoodInfo s;
    public static final FoodInfo t;
    public static final FoodInfo u;
    public static final FoodInfo v;
    public static final FoodInfo w;
    public static final FoodInfo x;
    public static final FoodInfo y;
    public static final FoodInfo z;
    public static final FoodInfo A;
    public static final FoodInfo B;
    public static final FoodInfo C;
    public static final FoodInfo D;
    public static final FoodInfo E;
    public static final FoodInfo F;
    public static final FoodInfo G;
    public static final FoodInfo H;
    public static final FoodInfo I;
    public static final FoodInfo J;
    public static final FoodInfo K;
    public static final FoodInfo L;
    public static final FoodInfo M;

    private static FoodInfo a(int var0) {
        return (new a()).a(var0).a(0.6F).d();
    }

    static {
        h = (new a()).a(2).a(0.3F).a(new MobEffect(MobEffects.HUNGER, 600, 0), 0.3F).a().d();
        i = (new a()).a(4).a(0.3F).b().d();
        j = (new a()).a(2).a(0.1F).d();
        k = (new a()).a(8).a(0.8F).a().d();
        l = (new a()).a(6).a(0.6F).a().d();
        m = (new a()).a(5).a(0.6F).d();
        n = (new a()).a(6).a(0.8F).a().d();
        o = (new a()).a(8).a(0.8F).a().d();
        p = (new a()).a(5).a(0.6F).a().d();
        q = (new a()).a(6).a(0.8F).d();
        r = (new a()).a(2).a(0.1F).d();
        s = (new a()).a(1).a(0.3F).c().d();
        t = (new a()).a(4).a(1.2F).a(new MobEffect(MobEffects.REGENERATION, 400, 1), 1.0F).a(new MobEffect(MobEffects.RESISTANCE, 6000, 0), 1.0F).a(new MobEffect(MobEffects.FIRE_RESISTANCE, 6000, 0), 1.0F).a(new MobEffect(MobEffects.ABSORBTION, 2400, 3), 1.0F).b().d();
        u = (new a()).a(4).a(1.2F).a(new MobEffect(MobEffects.REGENERATION, 100, 1), 1.0F).a(new MobEffect(MobEffects.ABSORBTION, 2400, 0), 1.0F).b().d();
        v = (new a()).a(6).a(1.2F).d();
        w = (new a()).a(6).a(0.1F).d();
        x = (new a()).a(2).a(0.3F).d();
        y = a(6);
        z = (new a()).a(2).a(0.3F).a().d();
        A = (new a()).a(2).a(0.3F).a(new MobEffect(MobEffects.POISON, 100, 0), 0.6F).d();
        B = (new a()).a(3).a(0.3F).a().d();
        C = (new a()).a(1).a(0.3F).d();
        D = (new a()).a(1).a(0.1F).a(new MobEffect(MobEffects.POISON, 1200, 3), 1.0F).a(new MobEffect(MobEffects.HUNGER, 300, 2), 1.0F).a(new MobEffect(MobEffects.CONFUSION, 300, 1), 1.0F).d();
        E = (new a()).a(8).a(0.3F).d();
        F = (new a()).a(3).a(0.3F).a().d();
        G = a(10);
        H = (new a()).a(4).a(0.1F).a(new MobEffect(MobEffects.HUNGER, 600, 0), 0.8F).a().d();
        I = (new a()).a(2).a(0.1F).d();
        J = (new a()).a(2).a(0.8F).a(new MobEffect(MobEffects.POISON, 100, 0), 1.0F).d();
        K = a(6);
        L = (new a()).a(2).a(0.1F).d();
        M = (new a()).a(1).a(0.1F).d();
    }

    //ScandiCraft: @BriceFab Tarte de pyrite
    public static final FoodInfo PYRITE_TARTE = (new FoodInfo.a()).a(4).a(1.2F)
        .a(new MobEffect(MobEffects.FASTER_DIG, 1200, 1), 1.0F)
        .a(new MobEffect(MobEffects.NIGHT_VISION, 3600, 0), 1.0F)
        .a(new MobEffect(MobEffects.ABSORBTION, 1200, 1), 1.0F)
        .b().d();

}
