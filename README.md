# scandicraft_server
Paper server: https://github.com/PaperMC/Paper

## Installation from scratch (!IMPORTANT utiliser GIT BASH, MAVEN et INTELLIJI)
1. Ouvrir git bash
2. git clone https://github.com/PaperMC/Paper.git
3. vérifier que JDK est bien dans le PATH (commands: java -version et jar) !pas de jre dans path
4. avoir maven (mvn -v)
5. cd Paper/ && ./paper jar
6. ourir /Paper avec Intelliji

## Build
1. Ouvrir le volet "maven"
2. Paper-Parent -> Lifecycle -> package